const secrets		= require("./bot_secrets.json");
const config		= require("./config.json");

const Discord		= require('discord.js');
const logger		= require("./logger.js");
const mysql         = require('mysql2/promise');

const db_table_sys	= "mod_contest"
const db_table_con	= "con_contest"
const db_table_sta	= "con_state"
const db_table_sub	= "con_submission"
const secs_per_day	= 86400

var contests_struct = {}

/*
contests_struct : {
	guild_1 : {
		rem_channels   : [rem_chan_g1_c1, rem_chan_g1_c2],
		channels   :     [chan_g1_c1, chan_g1_c2],
		chan_g1_c1 :  {
			announcement: <announcement channel id>,
			management:   <management   channel id>,
			emoji  :      <contest emoji>,
			system :      <contest sys  >,
			tsp:          <tsp          >,
			state:        <state        >,
			id:           <contest id   >
		},
		chan_g1_c2 : {...},
		...
	guild_1 : {...},
	...
	}
}
*/

const mysq_data = {
    host:       secrets.mysql.host,
    port:       secrets.mysql.port,  
    user:       secrets.mysql.user,
    password:   secrets.mysql.pass,
    database:   secrets.mysql.db
}

function log_info(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.info(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_warn(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.warn(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}

function log_error(module, submodule, func, msg){
	message = `${msg}`.replace(/(\r\n|\n|\r)/gm, " ")
    console.log (`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
    logger.error(`{"module":"${module}","submodule":"${submodule}","function":"${func}","message":"${message}"}`)
}


// Establish Discord Connection
const client  = new Discord.Client({ intents: [Discord.GatewayIntentBits.Guilds, Discord.GatewayIntentBits.GuildMessages, Discord.GatewayIntentBits.MessageContent, Discord.GatewayIntentBits.GuildMessageReactions] })
client.login(secrets.discord)

client.on('ready', bot => {
	log_info('PL-Contest', 'client.on', 'ready', 'Online')

	setInterval(contest_loop, 120 * 1000);
	load_submissions()
});

client.on('warn', warn => {
	log_warn('PL-Contest', 'client.on', 'warn', warn.stack)
});

client.on('error', err => {
	log_error('PL-Contest', 'client.on', 'error', err.stack)
});

client.on('messageCreate', message => {
	if(message.author.bot){return}
	if(contests_struct[message.guildId] == undefined){return}

	if(contests_struct[message.guildId].channels.includes(message.channelId))
	{
		con_add_sub(message, contests_struct[message.guildId][message.channelId].id)
	}
});

client.on("messageDelete", message => {
	if(message.author.bot){return}
	if(contests_struct[message.guildId] == undefined){return}

	if(contests_struct[message.guildId].channels.includes(message.channelId))
	{
		con_rem_sub(message, contests_struct[message.guildId][message.channelId].id)
	}
});

client.on('messageReactionAdd', (reaction, user) => {
	if(user.bot == true){return}

	if (contests_struct[reaction.message.guildId].rem_channels.includes(reaction.message.channelId)){
		rem_req_rem(reaction)
	}
	else if(contests_struct[reaction.message.guildId].channels.includes(reaction.message.channelId))
	{
		if(reaction.message.author.id == user.id){
			reaction.users.remove(user.id).catch(error => {
				log_error('PL-Contest',	'message reaction add', 'remove self react error', error.stack)
			})
		}
	}
});

client.on("interactionCreate", async interaction => {
	try{
		if (!interaction.isCommand() && !interaction.isButton()){return}

		if(interaction.isCommand())
		{
			if(interaction.commandName == 'contest_sys'|| interaction.commandName == 'contest')
			{
				if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false && interaction.member.roles.cache.has(config[interaction.guildId].common.roles.contest) == false){
					interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
						log_error('PL-Contest', '', 'interaction reply', error.stack)
					})
					return
				}

				if(interaction.commandName == 'contest_sys')
				{
					contest_sys(interaction)
				}
				else if (interaction.commandName == 'contest')
				{
					contest(interaction)
				}
			}
		}
		else if (interaction.isButton())
		{
			if(interaction.customId.startsWith('contest'))
			{
				if(interaction.customId.startsWith('contest_res'))
				{
					btn_contest_results(interaction)
				}
				else if (interaction.customId.startsWith('contest_theme'))
				{
					btn_theme_list(interaction)
				}
				else
				{
					if(interaction.member.roles.cache.has(config[interaction.guildId].common.roles.admin) == false && interaction.member.roles.cache.has(config[interaction.guildId].common.roles.contest) == false){
						interaction.reply({ content:"You do not have the permissions to use this command", ephemeral: true }).catch(error => {
							log_error('PL-Contest', '', 'interaction reply', error.stack)
						})
						return
					}
					else
					{
						if(interaction.customId.startsWith('contest_sys'))
						{
							btn_contest_sys(interaction)
						}
						else if (interaction.customId.startsWith('contest'))
						{
							btn_contest(interaction)
						}
					}
				}
			}		
		}

	} catch (error){
		log_error('PL-Contest', '', 'interaction create', error.stack)
	}
});

/*------------------------------------------------------------------------------------------------------
	Contest System Interaction Handling
		- create contest system
		- remove contest system
		- update contest system
		- show   contest system


Contest System Mainpage
┌───────────────────┐
│ Contest System    │
│ Page: 1/3         │
│ Id     Name       │
│ 10     Name_10    │
│  9     Name_9     │
│  8     Name_8     │
│  7     Name_7     │
│  6     Name_6     │
│  5     Name_5     │
│  4     Name_4     │
│  3     Name_3     │
│  2     Name_2     │
│  1     Name_1     │
│  [sys|1|1]        │
└───────────────────┘
            ┌ Shows Contest System Subpage with id 8
┌───┐┌───┐┌───┐┌───┐┌───┐
│ 10││ 9 ││ 8 ││ 7 ││ 6 │
└───┘└───┘└───┘└───┘└───┘
┌───┐┌───┐┌───┐┌───┐┌───┐
│ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
└───┘└───┘└───┘└───┘└───┘
┌───┐┌───┐┌───┐┌───┐┌───┐
│ « ││ < ││ _ ││ » ││ > │
└───┘└───┘└───┘└───┘└───┘
  └ Page Change  

Contest System Subpage
┌────────────────────────┐
│ Contest System Subpage │
│ Id:                 10 │
│ Name:          Theme10 │
│ Emoji:           <#id> │
│ Channels:              │
│ Management:      <#id> │
│ Announcement:    <#id> │
│ Submission:      <#id> │
│ Removal:         <#id> │
│ Removal Count:       3 │
│ Durations:             │
│ Contest:            14 │
│ Submissions:        2  │
│ Voting:             2  │
│ [sys|1|1]              │
└────────────────────────┘
┌───────┐
│ Back  │
└───────┘
   │  
   └ Back to Contest System Mainpage

  ------------------------------------------------------------------------------------------------------
*/

async function contest_sys(interaction){
	try {
		if      (interaction.options.getSubcommand() === 'create'){contest_sys_create(interaction)}
		else if (interaction.options.getSubcommand() === 'remove'){contest_sys_remove(interaction)}
		else if (interaction.options.getSubcommand() === 'update'){contest_sys_update(interaction)}
		else if (interaction.options.getSubcommand() === 'show'  ){contest_sys_show(interaction)}
		else {
			log_error('PL-Contest', 'contest sys', 'contest_sys ', 'Unknown Interaction: ' + interaction)
			interaction.reply({ content:"Unknown command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys', 'interaction reply', error.stack)
			})
			return
		}
	} catch (error){
		log_error('PL-Contest', 'contest sys ', 'contest_sys', error.stack)
	}
}

async function contest_sys_create(interaction){
	try {
		let name	= interaction.options.getString ('name'					)
		let ann_cha	= interaction.options.getChannel('announcement_channel'	)
		let man_cha	= interaction.options.getChannel('management_channel'	)
		let sub_cha	= interaction.options.getChannel('submission_channel'	)
		let rem_req	= interaction.options.getChannel('removal_req_channel'	)
		let rem_res	= interaction.options.getChannel('removal_res_channel'	)
		let rem_cnt	= interaction.options.getInteger('removal_count'		)
		let con_dur	= interaction.options.getInteger('contest_duration'		)
		let sub_dur	= interaction.options.getInteger('submission_duration'	)
		let vot_dur	= interaction.options.getInteger('voting_duration'		)
		let emoji	= interaction.options.getString ('emoji'				)

		if(name == null || ann_cha == null || man_cha == null || sub_cha == null || rem_req == null || rem_res == null || rem_cnt == null || con_dur == null || sub_dur == null || vot_dur == null || emoji == null ){
			interaction.reply({ content:"Not all parameters were provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys create', 'interaction reply', error.stack)})
			return   		
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);
		const [results, fields] = await mysql_conn.execute('SELECT id FROM ' + db_table_sys + ' WHERE guild = ? AND submission_chan = ? ', [interaction.guildId, sub_cha.id])

		if(results.length < 1)
		{
			const [results_sys, fields_sys] = await mysql_conn.execute('INSERT INTO ' + db_table_sys + ' (guild, name, announcement_chan, management_chan, submission_chan, contest_duration, sub_duration, vote_duration, contest_emoji, removal_req_chan, removal_res_chan, removal_cnt) VALUES (?,?,?,?,?,?,?,?,?,?,?,?)', [interaction.guildId, name, ann_cha.id, man_cha.id, sub_cha.id, con_dur, sub_dur, vot_dur, emoji, rem_req.id, rem_res.id, rem_cnt])
			const [results_sta, fields_sta] = await mysql_conn.execute('INSERT INTO ' + db_table_sta + ' (contest_sys, contest, state) VALUES (?,0,4)', [results_sys.insertId])

			if(results_sys.affectedRows == 1 && results_sta.affectedRows == 1)
			{
				interaction.reply({ content:"Contest System creation  was  successfull.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys create', 'interaction reply', error.stack)})
			}
			else
			{
				interaction.reply({ content:"Something went wrong during Contest System creation.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys create', 'interaction reply', error.stack)})
			}
		}
		else
		{
			interaction.reply({ content:"Contest System is already operating on this submission channel.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys create', 'interaction reply', error.stack)
			})  
			return
		}

		mysql_conn.end()

    } catch (error){
        log_error('PL-Contest', 'contest sys create', 'contest sys create', error.stack)
    }
}

async function contest_sys_remove(interaction){
	try {
		let id	= interaction.options.getInteger('id')

		if(id == null){
			interaction.reply({ content:"Id must be provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys remove', 'interaction reply', error.stack)})
			return   		
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT id FROM ' + db_table_sys + ' WHERE guild = ? AND id = ? ', [interaction.guildId, id])
		
		if(results.length > 0)
		{

			const [results_sys, fields_sys] = await mysql_conn.execute('DELETE FROM ' + db_table_sys + ' WHERE id = ?', [id])
			const [results_sta, fields_sta] = await mysql_conn.execute('DELETE FROM ' +  db_table_sta + ' WHERE contest_sys = ?', [id])

			if(results_sys.affectedRows == 1 && results_sta.affectedRows == 1)
			{
				interaction.reply({ content:"Contest System was removed successfully.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys remove', 'interaction reply', error.stack)}) 
			}
			else
			{
				interaction.reply({ content:"Something went wrong during Contest System removal.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys create', 'interaction reply', error.stack)})
			}
		}
		else
		{
			interaction.reply({ content:"Contest System is with this id does not exist on this server.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys remove', 'interaction reply', error.stack)
			})  
			return
		}
		
		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest sys remove', 'contest sys remove', error.stack)
	}
}

async function contest_sys_update(interaction){
	try {
		let id	= interaction.options.getInteger('id')

		if(id == null){
			interaction.reply({ content:"Id must be provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys update', 'interaction reply', error.stack)})
			return   		
		}

		let name	= interaction.options.getString ('name'					)
		let ann_cha	= interaction.options.getChannel('announcement_channel'	)
		let man_cha	= interaction.options.getChannel('management_channel'	)
		let sub_cha	= interaction.options.getChannel('submission_channel'	)
		let rem_req	= interaction.options.getChannel('removal_req_channel'	)
		let rem_res	= interaction.options.getChannel('removal_res_channel'	)
		let rem_cnt	= interaction.options.getInteger('removal_count'		)
		let con_dur	= interaction.options.getInteger('contest_duration'		)
		let sub_dur	= interaction.options.getInteger('submission_duration'	)
		let vot_dur	= interaction.options.getInteger('voting_duration'		)
		let emoji	= interaction.options.getString ('emoji'				)

		if(name == null && ann_cha == null && man_cha == null && sub_cha == null && rem_req == null && rem_res == null && rem_cnt == null && con_dur == null && sub_dur == null && vot_dur == null && emoji == null ){
			interaction.reply({ content:"All optional parameters are empty -> nothing to update.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys update', 'interaction reply', error.stack)})
			return   		
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_sys + ' WHERE guild = ? AND id = ? ', [interaction.guildId, id])
		
		if(results.length > 0)
		{

			if(name		== null){name		= results[0]['name'				]}
			if(ann_cha	== null){ann_cha	= results[0]['announcement_chan']} else {ann_cha = ann_cha.id}
			if(man_cha	== null){man_cha	= results[0]['management_chan'	]} else {man_cha = man_cha.id}
			if(sub_cha	== null){sub_cha	= results[0]['submission_chan'	]} else {sub_cha = sub_cha.id}
			if(rem_req	== null){rem_req	= results[0]['removal_req_chan'	]} else {rem_req = rem_req.id}
			if(rem_res	== null){rem_res	= results[0]['removal_res_chan'	]} else {rem_res = rem_res.id}
			if(rem_cnt	== null){rem_cnt	= results[0]['removal_cnt'		]}
			if(con_dur	== null){con_dur	= results[0]['contest_duration'	]}
			if(sub_dur	== null){sub_dur	= results[0]['sub_duration'		]}
			if(vot_dur	== null){vot_dur	= results[0]['vote_duration'	]}
			if(emoji	== null){emoji		= results[0]['contest_emoji'	]}

			const [results_sys, fields_sys] = await mysql_conn.execute('UPDATE ' + db_table_sys + ' SET name = ?, announcement_chan = ?, management_chan = ?, submission_chan = ?, contest_duration = ?, sub_duration = ?, vote_duration = ?, contest_emoji = ?, removal_req_chan = ?, removal_res_chan = ?, removal_cnt = ? WHERE id = ?', [name, ann_cha, man_cha, sub_cha, con_dur, sub_dur, vot_dur, emoji, rem_req, rem_res, rem_cnt, id])

			if(results_sys.affectedRows == 1)
			{
				interaction.reply({ content:"Contest System was updated successfully.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys update', 'interaction reply', error.stack)})
			}
			else
			{
				interaction.reply({ content:"Something went wrong during updating the Contest System.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys update', 'interaction reply', error.stack)})
			}
		}
		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest sys update', 'contest sys update', error.stack)
	}
}

function contest_sys_show(interaction){
	try {
		args = {
			"page": 	null,
			"pages":	null
		}

		contest_sys_embed_main(interaction, args, true)
		
	} catch (error){
		log_error('PL-Contest', '', 'contest sys show', error.stack)
	}
}

async function contest_sys_embed_main(interaction, args, reply){
	/*
	Contest System Mainpage
	┌───────────────────────────────────────┐
	│ Contest System Mainpage               │
	│ Page: 1/3                             │
	│ Id     Name        Submission Channel │
	│ 10     Name_10                  <#id> │
	│  9     Name_9                   <#id> │
	│  8     Name_8                   <#id> │
	│  7     Name_7                   <#id> │
	│  6     Name_6                   <#id> │
	│  5     Name_5                   <#id> │
	│  4     Name_4                   <#id> │
	│  3     Name_3                   <#id> │
	│  2     Name_2                   <#id> │
	│  1     Name_1                   <#id> │
	│  [sys|1|1]                            │
	└───────────────────────────────────────┘
				┌ Shows Contest System Subpage with id 8
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ 10││ 9 ││ 8 ││ 7 ││ 6 │
	└───┘└───┘└───┘└───┘└───┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
	└───┘└───┘└───┘└───┘└───┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ > ││ » │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change  
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}
		let buttons = []
		let col1 	= ""
		let col2 	= ""
		let col3 	= ""

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_sys, fields] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table_sys + ' WHERE guild = ?', [interaction.guildId])

		if(results_sys[0]['COUNT(*)'] != 0)
		{
			args.pages = Math.ceil(results_sys[0]['COUNT(*)'] / 10).toFixed();

			const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_sys + ' WHERE guild = ? LIMIT 10 OFFSET '+ (args.page - 1)* 10, [interaction.guildId])

			if(results.length < 1){
				interaction.reply({ content:"No Contest systems on this server", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys show', 'interaction reply', error.stack)}) 
				return null
			}

			for (let i = 0; i < results.length && i < 10; i++) {	
				col1 = col1 + '`' + results[i]["id"             ] + '`\n'
				col2 = col2 + `${   results[i]["name"           ]}\n`
				col3 = col3 + `<#${ results[i]["submission_chan"]}>\n`
										
				IdButton = new Discord.ButtonBuilder().setCustomId("contest_sys_" + results[i]["id"].toString()).setLabel(results[i]["id"].toString()).setStyle(Discord.ButtonStyle.Secondary)
				buttons.push(IdButton)
			}

			contest_sys_first	= new Discord.ButtonBuilder().setCustomId('contest_sys_first').setLabel('<<').setStyle(Discord.ButtonStyle.Primary)
			contest_sys_prev	= new Discord.ButtonBuilder().setCustomId('contest_sys_prev' ).setLabel('<') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_mid		= new Discord.ButtonBuilder().setCustomId('contest_sys_mid'  ).setLabel('─') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_next	= new Discord.ButtonBuilder().setCustomId('contest_sys_next' ).setLabel('>') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_last	= new Discord.ButtonBuilder().setCustomId('contest_sys_last' ).setLabel('>>').setStyle(Discord.ButtonStyle.Primary)

			let contest_sys_embed = new  Discord.EmbedBuilder()
			.setTitle("Contest System Mainpage")
			.setColor(config[interaction.guildId].common.embed)
			.setDescription(`Page: ${args.page}/${args.pages}`)
			.setFooter({ text: `[${args.page}|${args.pages}]`})
			.addFields(
				{name:'Id',						value: col1, inline: true},
				{name:'Name',					value: col2, inline: true},
				{name:'Submission Chhannel',	value: col3, inline: true},
			)

			
			if(reply) // Reply with embed 
				if(results.length > 5){
					interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons.slice(0, 5)},{type: 1,  components:buttons.slice(5)},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest sys show', 'interaction reply', error.stack)
					})

				} else {
					interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest sys show', 'interaction reply', error.stack)
					})

				}
			else // Update embed 
			{
				if(results.length > 5){
					interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons.slice(0, 5)},{type: 1,  components:buttons.slice(5)},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest sys show', 'interaction update', error.stack)
					})

				} else {
					interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest sys show', 'interaction update', error.stack)
					})

				}
			}			

		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest sys show', 'contest sys embed main', error.stack)
		return null
	}
}

async function contest_sys_embed_sub(interaction, args, contest_sys_id){
	/*
	Contest System Subpage
	┌────────────────────────┐
	│ Contest System Subpage │
	│ Name:            Value │
	│ Id:                 10 │
	│ Name:          Theme10 │
	│ Emoji:           <#id> │
	│                        │
	│ Channels:              │
	│ Management:      <#id> │
	│ Announcement:    <#id> │
	│ Submission:      <#id> │
	│ Removal:         <#id> │
	│ Removal Count:       3 │
	│                        │
	│ Durations:             │
	│ Contest:            14 │
	│ Submissions:        2  │
	│ Voting:             2  │
	│ [sys|1|1]              │
	└────────────────────────┘
	┌───────┐
	│ Back  │
	└───────┘
	│  
	└ Back to Contest System Mainpage
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_sys + ' WHERE guild = ? AND id = ?', [interaction.guildId, contest_sys_id])

		if(results.length > 0)
		{			
			let contest_sys_embed = new Discord.EmbedBuilder()
			.setTitle("Contest System Subpage")
			.setColor(config[interaction.guildId].common.embed)
			.setFooter({ text: `[${args.page}|${args.pages}]`})
			.addFields(
				{name:'Name',	value: 'Name: \n id: \n\n Durations: \n Contest: \n Submissions: \n Voting: \n\n Channels: \n Management: \n Announcement: \n Submission: \n\n Submission Removal: \n Channel: \n Count: \n\n Emoji: ', inline: true},
				{name:'Value',	value: `${results[0]['name']}\n${results[0]['id']}\n\n\n${results[0]['contest_duration']}\n${results[0]['sub_duration']}\n${results[0]['vote_duration']}\n\n\n<#${results[0]['management_chan']}>\n<#${results[0]['announcement_chan']}>\n<#${results[0]['submission_chan']}>\n\n\n<#${results[0]['removal_req_chan']}>\n${results[0]['removal_cnt']}\n\n${results[0]['contest_emoji']}`, inline: true},
			)

			var contest_sys_button = new Discord.ButtonBuilder().setCustomId('contest_sys_back').setLabel('Back').setStyle(Discord.ButtonStyle.Primary)
			interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_button]}]} ).catch(error => {
				log_error('PL-Contest', 'contest sys show', 'interaction update', error.stack)
			})
		}
		else
		{
			interaction.reply({ content:"No Contest System is configured with this id on this server.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys show', 'interaction reply', error.stack)
			})  
			return null
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest sys show', 'contest sys embed sub', error.stack)
		return null
	}
}

function btn_contest_sys (interaction){
	try {
		let button	= interaction.customId.split("_")
		let footer	= interaction.message.embeds[0].footer.text.replace("[","").replace("]","").split("|")

		if(button.length != 3 && footer.length != 2){
			interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)
			})  
			return
		}

		args = {
			"page": 	parseInt(footer[0]),
			"pages":	parseInt(footer[1])
		}

		if(button[2] == "first"){
			if(args.page > 1){args.page = 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "next")
		{
			if(args.page < args.pages){args.page = args.page + 1}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "mid")
		{
			interaction.reply({ content:"This button is just for alignment.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)})
			return
		}
		else if(button[2] == "prev")
		{
			if(args.page > 1){args.page = args.page - 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "last")
		{
			if(args.page != args.pages){args.page = args.pages}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)})
				return
			}	
		}

		if(button[2] == "first" || button[2] == "next" || button[2] == "prev" || button[2] == "last" || button[2] == "back")
		{
			embed = contest_sys_embed_main(interaction, args, false)
		}
		else
		{
			embed = contest_sys_embed_sub(interaction, args, button[2])
		}

	} catch (error){
		log_error('PL-Contest', 'contest sys button', 'contest sys button', error.stack)
	}
}

/*
------------------------------------------------------------------------------------------------------
	Contest Interaction Handling
		- create contest
		- remove contest
		- update contest
		- show   contest

┌─────────────────────────────────────────┐
│ Contest List Main Page                  │
│ Id | Theme     Startdate           Link │
│ 10 | Theme10   October 19, 2022    Link │
│  9 | Theme9    October 18, 2022    Link │
│  8 | Theme8    October 17, 2022    Link │
│  7 | Theme7    October 16, 2022    Link │
│  6 | Theme6    October 15, 2022    Link │
│  5 | Theme5    October 14, 2022    Link │
│  4 | Theme4    October 13, 2022    Link │
│  3 | Theme3    October 12, 2022    Link │
│  2 | Theme2    October 11, 2022    Link │
│  1 | Theme1    October 10, 2022    Link │
└─────────────────────────────────────────┘
            ┌ Shows Contest System Subpage with id 8
┌───┐┌───┐┌───┐┌───┐┌───┐
│ 10││ 9 ││ 8 ││ 7 ││ 6 │
└───┘└───┘└───┘└───┘└───┘
┌───┐┌───┐┌───┐┌───┐┌───┐
│ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
└───┘└───┘└───┘└───┘└───┘
┌───┐┌───┐┌───┐┌───┐┌───┐
│ « ││ < ││ _ ││ » ││ > │
└───┘└───┘└───┘└───┘└───┘
  └ Page Change  

┌─────────────────────────────────────────┐
│ Contest Subpage                         │
│ Id:    10 [x]                           │
│ Theme: Theme10                          │
│ Start: October 10, 2022 10:00 AM        │
│ Vote:  October 17, 2022 10:00 AM        │
| End:   October 19, 2022 10:00 AM        │
│ Link:  Msg Link                         │
└─────────────────────────────────────────┘
┌───┐┌────┐
│ < ││ <┘ │
└───┘└────┘
  │     │  
  │     └ Back to Contest List Main Page
  └ Prev Contest Subpage		
------------------------------------------------------------------------------------------------------
*/

function contest(interaction){
	try {
		if      (interaction.options.getSubcommand() === 'create'         ){contest_create(interaction)}
		else if (interaction.options.getSubcommand() === 'remove'         ){contest_remove(interaction)}
		else if (interaction.options.getSubcommand() === 'update'         ){contest_update(interaction)}
		else if (interaction.options.getSubcommand() === 'show'           ){contest_show  (interaction)}
		else if (interaction.options.getSubcommand() === 'list'           ){theme_list    (interaction)}
		else if (interaction.options.getSubcommand() === 'removal_request'){issue_req_rem (interaction)}
		else {
			log_error('PL-Contest', 'contest', 'contest ', 'Unknown Interaction: ' + interaction)
			interaction.reply({ content:"Unknown command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest', 'interaction reply', error.stack)
			})
			return
		}
	} catch (error){
		log_error('PL-Contest', 'contest', 'contest', error.stack)
	}
}

async function contest_create(interaction){
	try {
		let con_sys	= interaction.options.getInteger('contest_system'		)
		let theme	= interaction.options.getString ('theme'				)
		let link	= interaction.options.getString ('message_link'			)
		let sta_tsp	= interaction.options.getInteger('start_timestamp'		)
		let sub_dur	= interaction.options.getInteger('submission_duration'	)
		let vot_dur	= interaction.options.getInteger('voting_duration'		)


		if(con_sys == null){
			interaction.reply({ content:"Contest System parameter was not provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest create', 'interaction reply', error.stack)})
			return
		}

		if(theme == null){
			interaction.reply({ content:"Theme parameter was not provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest create', 'interaction reply', error.stack)})
			return
		}

		if(theme == null){
			interaction.reply({ content:"Link parameter was not provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest create', 'interaction reply', error.stack)})
			return
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_sys, fields_sys] = await mysql_conn.execute('SELECT * FROM ' + db_table_sys + ' WHERE guild = ? AND id = ? ', [interaction.guildId, con_sys])

		if(results_sys.length > 0)
		{
			const [results_tsp, fields_tsp] = await mysql_conn.execute('SELECT max(start_tsp) FROM ' + db_table_con + ' WHERE contest_sys = ?',[con_sys])

			if(results_tsp.length > 0)
			{
				if(sta_tsp == null){sta_tsp = new Date(results_tsp[0]["max(start_tsp)"]).getTime() / 1000 + results_sys[0]['contest_duration'] * secs_per_day}
				if(sub_dur == null){sub_dur = results_sys[0]['sub_duration' ]}
				if(vot_dur == null){vot_dur = results_sys[0]['vote_duration']}

				//Tsp to Mysql Date Time Conversion
				let start_tsp = new Date(sta_tsp * 1000);
				start_tsp = start_tsp.toISOString().split('T')[0] + ' ' + start_tsp.toTimeString().split(' ')[0]

				let vote_tsp = new Date((sta_tsp +  sub_dur * secs_per_day) * 1000 );
				vote_tsp = vote_tsp.toISOString().split('T')[0] + ' ' + vote_tsp.toTimeString().split(' ')[0]

				let end_tsp = new Date((sta_tsp + sub_dur * secs_per_day + vot_dur * secs_per_day) * 1000);
				end_tsp = end_tsp.toISOString().split('T')[0] + ' ' + end_tsp.toTimeString().split(' ')[0]

				const [results_con, fields_con] = await mysql_conn.execute('INSERT INTO ' + db_table_con + ' (contest_sys, theme, link, completed, start_tsp, vote_tsp, end_tsp) VALUES (?,?,?,?,?,?,?)', [con_sys, theme, link, 0, start_tsp, vote_tsp, end_tsp])

				if(results_con.affectedRows == 1)
				{
					interaction.reply({ content:"Contest creation  was  successfull.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest create', 'interaction reply', error.stack)})
				}
				else
				{
					interaction.reply({ content:"Something went wrong during Contest creation.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest create', 'interaction reply', error.stack)})
				}
			}
		}

		mysql_conn.end()

    } catch (error){
        log_error('PL-Contest', 'contest create', 'contest create', error.stack)
    }
}

async function contest_remove(interaction){
	try {
		let id = interaction.options.getInteger('id')

		if(id == null){
			interaction.reply({ content:"Id must be provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest remove', 'interaction reply', error.stack)})
			return
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_con, fields_con] = await mysql_conn.execute('SELECT id FROM ' + db_table_con + ' WHERE id = ?', [id])

		if(results_con.length > 0)
		{
			const [results_del, fields_del] = await mysql_conn.execute('DELETE FROM ' + db_table_con + ' WHERE id = ?', [id])

			if(results_del.affectedRows == 1)
			{
				interaction.reply({ content:"Contest was removed successfully.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest remove', 'interaction reply', error.stack)})
			}
			else
			{
				interaction.reply({ content:"Something went wrogn during Contest removal.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest remove', 'interaction reply', error.stack)})
			}
		}
		else
		{
			interaction.reply({ content:"Contest with this id does not exist.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest remove', 'interaction reply', error.stack)
			})
			return
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest remove', 'contest remove', error.stack)
	}
}

async function contest_update(interaction){
	try {
		let id		= interaction.options.getInteger('id'					)
		let theme	= interaction.options.getString ('theme'				)
		let link	= interaction.options.getString ('message_link'			)
		let sta_tsp	= interaction.options.getInteger('start_timestamp'		)
		let sub_dur	= interaction.options.getInteger('submission_duration'	)
		let vot_dur	= interaction.options.getInteger('voting_duration'		)


		if(id == null){
			interaction.reply({ content:"Id must be provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
			return
		}

		if(theme == null && link == null && sta_tsp == null && sub_dur == null && vot_dur == null){
			interaction.reply({ content:"No parameter was not provided -> nothing to update", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
			return
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_con + ' WHERE id = ?', [id])

		if(results.length > 0)
		{
			if(theme == null){theme = results[0]['theme']}
			if(link  == null){link  = results[0]['link']}

			if(sta_tsp == null && sub_dur == null && vot_dur == null)
			{
				const [results_upd, fields_upd] = await mysql_conn.execute('UPDATE ' + db_table_con + ' SET theme = ?, link = ? WHERE id = ?', [theme, link, id])

				if(results_upd.affectedRows == 1)
				{
					interaction.reply({ content:"Contest was updated successfully.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
				}
				else
				{
					interaction.reply({ content:"Something went wrong during updating the Contest.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
				}
			}
			else
			{
				if(sta_tsp == null){sta_tsp =  new Date(results[0]["start_tsp"]).getTime() / 1000}
				if(sub_dur == null){sub_dur = (new Date(results[0]["vote_tsp" ]).getTime() - new Date(results[0]["start_tsp"]).getTime()) / (secs_per_day * 1000)}
				if(vot_dur == null){vot_dur = (new Date(results[0]["end_tsp"  ]).getTime() - new Date(results[0]["vote_tsp" ]).getTime()) / (secs_per_day * 1000)}

				//Tsp to Mysql Date Time Conversion
				let start_tsp = new Date(sta_tsp * 1000);
				start_tsp = start_tsp.toISOString().split('T')[0] + ' ' + start_tsp.toTimeString().split(' ')[0]

				let vote_tsp = new Date((sta_tsp +  sub_dur * secs_per_day) * 1000 );
				vote_tsp = vote_tsp.toISOString().split('T')[0] + ' ' + vote_tsp.toTimeString().split(' ')[0]

				let end_tsp = new Date((sta_tsp + sub_dur * secs_per_day + vot_dur * secs_per_day) * 1000);
				end_tsp = end_tsp.toISOString().split('T')[0] + ' ' + end_tsp.toTimeString().split(' ')[0]

				const [results_upd, fields_upd] = await mysql_conn.execute('UPDATE ' + db_table_con + ' SET theme = ?, link = ?, start_tsp = ?, vote_tsp = ?, end_tsp = ? WHERE id = ?', [theme, link, start_tsp, vote_tsp, end_tsp, id])

				if(results_upd.affectedRows == 1)
				{
					interaction.reply({ content:"Contest was updated successfully.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
				}
				else
				{
					interaction.reply({ content:"Something went wrong during updating the Contest.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)})
				}
			}
		}
		else
		{
			interaction.reply({ content:"Contest with this id does not exist.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest update', 'interaction reply', error.stack)
			})  
			return			
		}

		mysql_conn.end()

    } catch (error){
        log_error('PL-Contest', 'contest update', 'contest update', error.stack)
    }
}

function contest_show(interaction){
	try {
		let cont_sys	= interaction.options.getInteger('contest_system')
		let show_all	= interaction.options.getBoolean('show_all')

		if(cont_sys == null){
			interaction.reply({ content:"Contest System parameter was not provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest show', 'interaction reply', error.stack)})
			return
		}

		args = {
			"sys"	:	cont_sys,
			"page"	: 	null,
			"pages"	:	null,
			"all"	:	0
		}

		if(show_all == true){args.all = 1}

		contest_embed_main(interaction, args, true)
		
	} catch (error){
		log_error('PL-Contest', '', 'contest sys show', error.stack)
	}
}

async function contest_embed_main(interaction, args, reply){
	/*
	┌─────────────────────────────────────────┐
	│ Contest List Main Page                  │
	│ Id | Theme     Startdate           Link │
	│ 10 | Theme10   October 19, 2022    Link │
	│  9 | Theme9    October 18, 2022    Link │
	│  8 | Theme8    October 17, 2022    Link │
	│  7 | Theme7    October 16, 2022    Link │
	│  6 | Theme6    October 15, 2022    Link │
	│  5 | Theme5    October 14, 2022    Link │
	│  4 | Theme4    October 13, 2022    Link │
	│  3 | Theme3    October 12, 2022    Link │
	│  2 | Theme2    October 11, 2022    Link │
	│  1 | Theme1    October 10, 2022    Link │
	└─────────────────────────────────────────┘
				┌ Shows Contest System Subpage with id 8
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ 10││ 9 ││ 8 ││ 7 ││ 6 │
	└───┘└───┘└───┘└───┘└───┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ 5 ││ 4 ││ 3 ││ 2 ││ 1 │
	└───┘└───┘└───┘└───┘└───┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ » ││ > │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change 
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}
		let buttons = []
		let col1 	= ""
		let col2 	= ""
		let col3 	= ""

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_cnt, fields_cnt] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table_con + ' WHERE contest_sys = ?', [args.sys])

		if(results_cnt[0]['COUNT(*)'] != 0)
		{
			args.pages = Math.ceil(results_cnt[0]['COUNT(*)'] / 10).toFixed();

			let query_str = ""
			if(args.all){
				query_str = 'SELECT * FROM ' + db_table_con + ' WHERE contest_sys = ? ORDER BY start_tsp DESC LIMIT 10 OFFSET ' + ((args.page - 1)* 10)
			} else {
				query_str = 'SELECT * FROM ' + db_table_con + ' WHERE contest_sys = ? AND completed = 0 ORDER BY start_tsp DESC LIMIT 10 OFFSET ' + ((args.page - 1)* 10)
			}

			const [results, fields] = await mysql_conn.execute(query_str, [args.sys])

			if(results.length < 1){
				interaction.reply({ content:"No Contests in this Queue", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest show', 'interaction reply', error.stack)}) 
				return null
			}

			for (let i = 0; i < results.length && i < 10; i++) {	
				col1 = col1 + '`' + results[i]["id"] + '`' + `| ${results[i]["theme"]}\n`
				col2 = col2 + `<t:${new Date(results[i]["start_tsp"]).getTime() / 1000}:f>\n`
				col3 = col3 + `[Link](${ results[i]["link"]})\n`
										
				IdButton = new Discord.ButtonBuilder().setCustomId("contest_" + results[i]["id"].toString()).setLabel(results[i]["id"].toString()).setStyle(Discord.ButtonStyle.Secondary)
				buttons.push(IdButton)
			}

			contest_sys_first	= new Discord.ButtonBuilder().setCustomId('contest_first').setLabel('<<').setStyle(Discord.ButtonStyle.Primary)
			contest_sys_prev	= new Discord.ButtonBuilder().setCustomId('contest_prev' ).setLabel('<') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_mid		= new Discord.ButtonBuilder().setCustomId('contest_mid'  ).setLabel('─') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_next	= new Discord.ButtonBuilder().setCustomId('contest_next' ).setLabel('>') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_last	= new Discord.ButtonBuilder().setCustomId('contest_last' ).setLabel('>>').setStyle(Discord.ButtonStyle.Primary)

			let contest_sys_embed = new  Discord.EmbedBuilder()
			.setTitle("Contest System Mainpage")
			.setColor(config[interaction.guildId].common.embed)
			.setDescription(`Page: ${args.page}/${args.pages}`)
			.setFooter({ text: `[${args.sys}|${args.page}|${args.pages}|${args.all}]`})
			.addFields(
				{name:'Id | Theme',		value: col1, inline: true},
				{name:'Startdate',		value: col2, inline: true},
				{name:'Link',			value: col3, inline: true},
			)

			if(reply) // Reply with embed 
				if(results.length > 5){
					interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons.slice(0, 5)},{type: 1,  components:buttons.slice(5)},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest show', 'interaction reply', error.stack)
					})

				} else {

					interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest show', 'interaction reply', error.stack)
					})

				}
			else // Update embed 
			{
				if(results.length > 5){
					interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons.slice(0, 5)},{type: 1,  components:buttons.slice(5)},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest show', 'interaction update', error.stack)
					})

				} else {
					interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:buttons},{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
						log_error('PL-Contest', 'contest show', 'interaction update', error.stack)
					})

				}						
			}

		}
		else
		{
			interaction.reply({ content:"No Contests in queue of this contest system.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys show', 'interaction reply', error.stack)
			})  
			return null
		}


		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest sys show', 'contest embed main', error.stack)
		return null
	}
}

async function contest_embed_sub(interaction, args, contest_id){
	/*
	┌─────────────────────────────────────────┐
	│ Contest Subpage                         │
	│ Id:    10 [x]                           │
	│ Theme: Theme10                          │
	│ Start: October 10, 2022 10:00 AM        │
	│ Vote:  October 17, 2022 10:00 AM        │
	| End:   October 19, 2022 10:00 AM        │
	│ Link:  Msg Link                         │
	└─────────────────────────────────────────┘
	┌───────┐
	│ Back  │
	└───────┘
	   │  
	   └ Back to Contest System Mainpage
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_con + ' WHERE id = ?', [contest_id])

		if(results.length > 0)
		{
			let comp	= " "
			if(results[0]["completed"] == 1){comp = "X"}
			let sta_tsp = new Date(results[0]["start_tsp"]).getTime() / 1000
			let vot_tsp = new Date(results[0]["vote_tsp" ]).getTime() / 1000
			let end_tsp = new Date(results[0]["end_tsp"  ]).getTime() / 1000

			let contest_sys_embed = new Discord.EmbedBuilder()
			.setTitle("Contest List Subpage")
			.setColor(config[interaction.guildId].common.embed)
			.setFooter({ text: `[${args.sys}|${args.page}|${args.pages}|${args.all}]`})
			.addFields(
				{name:'Name',	value: 'id: \n Theme: \n Start: \n Vote: \n End: \n Link:', inline: true},
				{name:'Value',	value: `${results[0]['id']} [${comp}]\n${results[0]['theme']}\n<t:${sta_tsp}:f>\n<t:${vot_tsp}:f>\n<t:${end_tsp}:f>\n[Link](${results[0]["link"]})`, inline: true},
			)

			var contest_sys_button = new Discord.ButtonBuilder().setCustomId('contest_back').setLabel('Back').setStyle(Discord.ButtonStyle.Primary)
			interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_button]}]} ).catch(error => {
				log_error('PL-Contest', 'contest show', 'interaction update', error.stack)
			})
		}
		else
		{
			interaction.reply({ content:"Contest was deleted.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest show', 'interaction reply', error.stack)
			})  
			return null
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest show', 'contest embed sub', error.stack)
		return null
	}
}

function btn_contest (interaction){
	try {
		let button	= interaction.customId.split("_")
		let footer	= interaction.message.embeds[0].footer.text.replace("[","").replace("]","").split("|")


		if(button.length != 2 && footer.length != 4){
			interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest sys button', 'interaction reply', error.stack)
			})  
			return
		}

		args = {
			"sys"	:	parseInt(footer[0]),
			"page"	: 	parseInt(footer[1]),
			"pages"	:	parseInt(footer[2]),
			"all"	:	parseInt(footer[3])
		}

		if(button[1] == "first"){
			if(args.page > 1){args.page = 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[1] == "next")
		{
			if(args.page < args.pages){args.page = args.page + 1}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[1] == "mid")
		{
			interaction.reply({ content:"This button is just for alignment.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest button', 'interaction reply', error.stack)})
			return
		}
		else if(button[1] == "prev")
		{
			if(args.page > 1){args.page = args.page - 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[1] == "last")
		{
			if(args.page != args.pages){args.page = args.pages}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest button', 'interaction reply', error.stack)})
				return
			}
		}

		if(button[1] == "first" || button[1] == "next" || button[1] == "prev" || button[1] == "last" || button[1] == "back")
		{
			embed = contest_embed_main(interaction, args, false)
		}
		else
		{
			embed = contest_embed_sub(interaction, args, button[1])
		}

	} catch (error){
		log_error('PL-Contest', 'contest button', 'contest button', error.stack)
	}
}


function theme_list(interaction){
	try {
		let cont_sys	= interaction.options.getInteger('contest_system')
		let show_all	= interaction.options.getBoolean('show_all')

		if(cont_sys == null){
			interaction.reply({ content:"Contest System parameter was not provided", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list', 'interaction reply', error.stack)})
			return
		}

		args = {
			"sys"	:	cont_sys,
			"page"	: 	null,
			"pages"	:	null,
			"all"	:	0
		}

		if(show_all == true){args.all = 1}

		theme_list_embed(interaction, args, true)
		
	} catch (error){
		log_error('PL-Contest', '', 'contest list', error.stack)
	}
}

async function theme_list_embed(interaction, args, reply){
	/*
	┌───────────────────┐
	│ Contest Themes    │
	│ Nr        Theme   │
	│  1        Theme1  │
	│  2        Theme2  │
	│  3        Theme3  │
	│  4        Theme4  │
	│  5        Theme5  │
	│  6        Theme6  │
	│  7        Theme7  │
	│  8        Theme8  │
	│  9        Theme9  │
	│ 10        Theme10 │
	└───────────────────┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ » ││ > │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change 
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}
		let buttons = []
		let col1 	= ""
		let col2 	= ""

		var mysql_conn = await  mysql.createConnection(mysq_data);

		let cnt_query_str = ""
		if(args.all){
			cnt_query_str = 'SELECT COUNT(*) FROM ' + db_table_con + ' WHERE contest_sys = ?'
		} else {
			cnt_query_str = 'SELECT COUNT(*) FROM ' + db_table_con + ' WHERE contest_sys = ? AND completed = 1'
		}
		const [results_cnt, fields_cnt] = await mysql_conn.execute(cnt_query_str, [args.sys])

		if(results_cnt[0]['COUNT(*)'] != 0)
		{
			args.pages = Math.ceil(results_cnt[0]['COUNT(*)'] / 10).toFixed();

			let query_str = ""
			if(args.all){
				query_str = 'SELECT theme, ROW_NUMBER() OVER() as row_num FROM ' + db_table_con + ' WHERE contest_sys = ? ORDER BY start_tsp ASC LIMIT 10 OFFSET ' + ((args.page - 1)* 10)
			} else {
				query_str = 'SELECT theme, ROW_NUMBER() OVER() as row_num FROM ' + db_table_con + ' WHERE contest_sys = ? AND completed = 1 ORDER BY start_tsp ASC LIMIT 10 OFFSET ' + ((args.page - 1)* 10)
			}

			const [results, fields] = await mysql_conn.execute(query_str, [args.sys])

			if(results.length < 1){
				interaction.reply({ content:"No Themes in this Contest List", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list', 'interaction reply', error.stack)}) 
				return null
			}

			for (let i = 0; i < results.length && i < 10; i++) {	
				col1 = col1 + `${results[i]["row_num"]}\n`
				col2 = col2 + `${results[i]["theme"]  }\n`
			}

			contest_sys_first	= new Discord.ButtonBuilder().setCustomId('contest_theme_first').setLabel('<<').setStyle(Discord.ButtonStyle.Primary)
			contest_sys_prev	= new Discord.ButtonBuilder().setCustomId('contest_theme_prev' ).setLabel('<') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_mid		= new Discord.ButtonBuilder().setCustomId('contest_theme_mid'  ).setLabel('─') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_next	= new Discord.ButtonBuilder().setCustomId('contest_theme_next' ).setLabel('>') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_last	= new Discord.ButtonBuilder().setCustomId('contest_theme_last' ).setLabel('>>').setStyle(Discord.ButtonStyle.Primary)

			let contest_sys_embed = new  Discord.EmbedBuilder()
			.setTitle("Contest Themes")
			.setColor(config[interaction.guildId].common.embed)
			.setDescription(`Page: ${args.page}/${args.pages}`)
			.setFooter({ text: `[${args.sys}|${args.page}|${args.pages}|${args.all}]`})
			.addFields(
				{name:'Nr.',	value: col1, inline: true},
				{name:'Theme',	value: col2, inline: true},
			)

			if(reply) // Reply with embed
			{
				interaction.reply({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Contest', 'contest list', 'interaction reply', error.stack)
				})
			}
			else // Update embed 
			{
				interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Contest', 'contest list', 'interaction update', error.stack)
				})			
			}

		}
		else
		{
			interaction.reply({ content:"No Themes in contest list.", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest list', 'interaction reply', error.stack)
			})  
			return null
		}


		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest list', 'ctheme_list_embed', error.stack)
		return null
	}
}

function btn_theme_list (interaction){
	try {
		let button	= interaction.customId.split("_")
		let footer	= interaction.message.embeds[0].footer.text.replace("[","").replace("]","").split("|")


		if(button.length != 2 && footer.length != 4){
			interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)
			})  
			return
		}

		args = {
			"sys"	:	parseInt(footer[0]),
			"page"	: 	parseInt(footer[1]),
			"pages"	:	parseInt(footer[2]),
			"all"	:	parseInt(footer[3])
		}

		if(button[2] == "first"){
			if(args.page > 1){args.page = 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "next")
		{
			if(args.page < args.pages){args.page = args.page + 1}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "mid")
		{
			interaction.reply({ content:"This button is just for alignment.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)})
			return
		}
		else if(button[2] == "prev")
		{
			if(args.page > 1){args.page = args.page - 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)})
				return
			}
		}
		else if(button[2] == "last")
		{
			if(args.page != args.pages){args.page = args.pages}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest list button', 'interaction reply', error.stack)})
				return
			}
		}

		if(button[2] == "first" || button[2] == "next" || button[2] == "prev" || button[2] == "last" || button[2] == "back")
		{
			embed = theme_list_embed(interaction, args, false)
		}
		else
		{
			embed = theme_list_embed(interaction, args, button[2])
		}

	} catch (error){
		log_error('PL-Contest', 'ccontest list button', 'contest list button', error.stack)
	}
}



/*
------------------------------------------------------------------------------------------------------
	Contest Loop
------------------------------------------------------------------------------------------------------
*/

async function con_add_sub(message, contest)
{
	try {

		if(message.attachments.size > 0)
		{
			var mysql_conn = await  mysql.createConnection(mysq_data);

			const [results_cnt, fields_cnt] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table_sub + ' WHERE user = ? AND contest = ? ', [message.author.id, contest])

			if(results_cnt[0]['COUNT(*)'] == 0)
			{

				const [results, fields] = await mysql_conn.execute('INSERT INTO ' + db_table_sub + ' (contest, message, channel, user) VALUES (?,?,?,?)', [contest, message.id, message.channelId, message.author.id])
				
				if(results.affectedRows == 1)
				{
					log_info('PL-Contest', 'add sub', '', `${message.author.id} added submission ${message.id} to contest ${contest}`)
				}
				else
				{
					log_error('PL-Contest', 'add sub', '', `Adding submission ${message.id} to contest ${contest} by ${message.author.id} failed`)
				}
			}
			else
			{
				message.channel.send("You have already entered a Submission. Please remove your submission before you enter a new one.")
				.then(msg => {setTimeout(() => msg.delete(), 5000)}).catch(error => {log_error('PL-Contest', 'add sub', 'remove error message', error.stack)})
				message.delete()									.catch(error => {log_error('PL-Contest', 'add sub', 'remove message', error.stack)})
			}

			mysql_conn.end()

		}
		else
		{
			
			await message.reply("Sending text only in this chat is prohibited")
				.then(msg => {setTimeout(() => msg.delete(), 5000)}).catch(error => {log_error('PL-Contest', 'add sub', 'remove error message', error.stack)})
			message.delete()                                        .catch(error => {log_error('PL-Contest', 'add sub', 'remove message', error.stack)})
		}

	} catch (error){
		log_error('PL-Contest', 'add sub', 'add sub', error.stack)
	}
}

async function con_rem_sub(message, contest)
{
	try {

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_sub, fields_sub] = await mysql_conn.execute('SELECT id FROM ' + db_table_sub + ' WHERE message = ? AND contest = ?', [message.id, contest])

		if(results_sub.length > 0)
		{
			const [results_del, fields_del] = await mysql_conn.execute('DELETE FROM ' + db_table_sub + ' WHERE id = ?', [results_sub[0]["id"]])

			if(results_del.affectedRows == 1)
			{
				log_info('PL-Contest', 'rem sub', 'sucessful', `${message.author.id} removed submission ${message.id} from contest ${contest}`)
			}
			else
			{
				log_error('PL-Contest', 'add sub', '', `Submission removal ${message.id} to contest ${contest} by ${message.author.id} failed`)
			}

		}
		else
		{
			log_warn('PL-Contest', 'rem sub', '', `removing unknown message ${message.id} from ${message.author.id} in channel ${message.channelId}`)
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'rem sub', 'rem sub', error.stack)
	}
}

async function load_submissions()
{
	try {
		var temp_contests_struct = {}

		/*
			Little Triple Join Magic

			'SELECT con_state.contest_sys, con_state.contest,  mod_contest.guild, mod_contest.submission_chan, mod_contest.announcement_chan, mod_contest.management_chan, mod_contest.contest_emoji, con_contest.start_tsp, con_contest.vote_tsp, con_contest.end_tsp, con_state.state, mod_contest.announcement_chan FROM con_state 
			INNER JOIN mod_contest ON con_state.contest_sys = mod_contest.id
			INNER JOIN con_contest ON con_state.contest = con_contest.id'

			Result:
			[RowDataPacket {
			contest_sys:        <contest_sys id>,
			contest:            <contest     id>,
			guild:              <guild       id>,
			submission_chan:    <sub channel id>,
			announcement_chan:  <ann channel id>,
			management_chan:    <man channel id>,
			emoji:              <emoji         >,
			start_tsp:          <start      tsp>,
			vote_tsp:           <vote       tsp>,
			end_tsp:            <end        tsp>,
			state:              <state          >
			},....]

			to fill temp_contest
			
			Structure : {
				guild_1 : {
					rem_channels	: [rem_chan_g1_c1, rem_chan_g1_c2],
					channels   		: [chan_g1_c1, chan_g1_c2],
					chan_g1_c1 :  {
						announcement: <announcement channel id>,
						management:   <management   channel id>,
						emoji  :      <contest emoji>,
						system :      <contest sys  >,
						tsp:          <tsp          >,
						state:        <state        >,
						id:           <contest id   >
					},
					chan_g1_c2 : {...},
					...
				guild_1 : {...},
				...
			}
		*/

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT ' + db_table_sta + '.contest_sys, ' + db_table_sta + '.contest,  ' + db_table_sys + '.guild, ' + db_table_sys + '.submission_chan, ' + db_table_sys + '.announcement_chan, ' + db_table_sys + '.management_chan, '  + db_table_sys + '.contest_emoji, ' + db_table_con + '.start_tsp, ' + db_table_con + '.vote_tsp, ' + db_table_con + '.end_tsp, ' + db_table_sta + '.state, ' + db_table_sys + '.removal_req_chan FROM ' + db_table_sta + ' INNER JOIN ' + db_table_sys + ' ON ' + db_table_sta + '.contest_sys = ' + db_table_sys + '.id LEFT JOIN ' + db_table_con + ' ON ' + db_table_sta + '.contest = ' + db_table_con + '.id', [])
		
		// Build temp_contests_struct
		for (var i in results){

			if(temp_contests_struct.hasOwnProperty(results[i]['guild'])){
				
				// Check if two contest systems are deployed on the same submission channel
				if(temp_contests_struct[results[i]['guild']].channels.includes(results[i]['submission_chan']))
				{
					log_error('PL-Contest', 'load submissions', 'load submissions', 'Submission channel duplicate\n' +  results[i] + '\n' + temp_contests_struct)
				}
				else
				{
					temp_contests_struct[results[i]['guild']].channels.push(results[i]['submission_chan'])
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']] = {}

					if(!temp_contests_struct[results[i]['guild']].rem_channels.includes(results[i]['removal_req_chan']))
					{
						temp_contests_struct[results[i]['guild']].rem_channels.push(results[i]['removal_req_chan'])
					}

					if     (results[i]['state'] == 0){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['start_tsp']}
					else if(results[i]['state'] == 1){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['vote_tsp']}
					else if(results[i]['state'] == 2){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['end_tsp']}
					else                             { temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = 0}

					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].announcement = results[i]['announcement_chan']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].management   = results[i]['management_chan']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].emoji        = results[i]['contest_emoji']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].system       = results[i]['contest_sys']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].id           = results[i]['contest']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].state        = results[i]['state']
				}

			}
			else
			{
				temp_contests_struct[results[i]['guild']] = {}
				temp_contests_struct[results[i]['guild']].channels     = [results[i][ 'submission_chan']]
				temp_contests_struct[results[i]['guild']].rem_channels = [results[i]['removal_req_chan']]
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']] = {}

				if     (results[i]['state'] == 0){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['start_tsp']}
				else if(results[i]['state'] == 1){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['vote_tsp']}
				else if(results[i]['state'] == 2){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['end_tsp']}
				else                             { temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = 0}

				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].announcement = results[i]['announcement_chan']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].management   = results[i]['management_chan']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].emoji        = results[i]['contest_emoji']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].system       = results[i]['contest_sys']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].id           = results[i]['contest']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].state        = results[i]['state']
			}

		}

		contests_struct = temp_contests_struct

		// Load Messages
		try {
			Object.keys(temp_contests_struct).forEach( guild_id=> {
				temp_contests_struct[guild_id].channels.forEach(async channel_id => { 

					var guild     = client.guilds .cache.get(guild_id);
					var channel   = guild.channels.cache.get(channel_id);

					const [results_sub, fields_sub] = await mysql_conn.execute('SELECT * FROM ' + db_table_sub + ' WHERE contest = ?' , [temp_contests_struct[guild_id][channel_id].id])

					for (var i in results_sub){
						channel.messages.fetch(results_sub[i]['message']).catch(error => {log_error('PL-Contest', 'load submissions', 'fetch submissions', error.stack)})
					}
				})
			});
		} catch (error){
			log_error('PL-Contest', 'load submissions', 'loading submissions', error.stack)
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'load submissions', 'load submissions', error.stack)
	}
}

async function contest_loop(){

	try {

		// Update Contests Struct 
		var temp_contests_struct = {}

		/*
			Little Triple Join Magic

			'SELECT con_state.contest_sys, con_state.contest,  mod_contest.guild, mod_contest.submission_chan, mod_contest.announcement_chan, mod_contest.management_chan, mod_contest.contest_emoji, con_contest.start_tsp, con_contest.vote_tsp, con_contest.end_tsp, con_state.state FROM con_state 
			INNER JOIN mod_contest ON con_state.contest_sys = mod_contest.id
			INNER JOIN con_contest ON con_state.contest = con_contest.id'

			Result:
			[RowDataPacket {
			contest_sys:        <contest_sys id>,
			contest:            <contest     id>,
			guild:              <guild       id>,
			submission_chan:    <sub channel id>,
			announcement_chan:  <ann channel id>,
			management_chan:    <man channel id>,
			emoji:              <emoji         >,
			start_tsp:          <start      tsp>,
			vote_tsp:           <vote       tsp>,
			end_tsp:            <end        tsp>,
			state:              <state          >
			},....]

			to fill temp_contest
			
			Structure : {
				guild_1 : {
					channels   : [chan_g1_c1, chan_g1_c2],
					chan_g1_c1 :  {
						announcement: <announcement channel id>,
						management:   <management   channel id>,
						emoji  :      <contest emoji>,
						system :      <contest sys  >,
						tsp:          <tsp          >,
						state:        <state        >,
						id:           <contest id   >
					},
					chan_g1_c2 : {...},
					...
				guild_1 : {...},
				...
			}
		*/

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT ' + db_table_sta + '.contest_sys, ' + db_table_sta + '.contest,  ' + db_table_sys + '.guild, ' + db_table_sys + '.submission_chan, ' + db_table_sys + '.announcement_chan, ' + db_table_sys + '.management_chan, '  + db_table_sys + '.contest_emoji, ' + db_table_con + '.start_tsp, ' + db_table_con + '.vote_tsp, ' + db_table_con + '.end_tsp, ' + db_table_sta + '.state, ' + db_table_sys + '.removal_req_chan FROM ' + db_table_sta + ' INNER JOIN ' + db_table_sys + ' ON ' + db_table_sta + '.contest_sys = ' + db_table_sys + '.id LEFT JOIN ' + db_table_con + ' ON ' + db_table_sta + '.contest = ' + db_table_con + '.id', [])


		// Build temp_contests_struct
		for (var i in results){

			if(temp_contests_struct.hasOwnProperty(results[i]['guild'])){
				
				// Check if two contest systems are deployed on the same submission channel
				if(temp_contests_struct[results[i]['guild']].channels.includes(results[i]['submission_chan']))
				{
					log_error('PL-Contest', 'constest loop', 'constest loop', 'Submission channel duplicate\n' +  results[i] + '\n' + temp_contests_struct)
				}
				else
				{
					temp_contests_struct[results[i]['guild']].channels.push(results[i]['submission_chan'])
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']] = {}

					if(!temp_contests_struct[results[i]['guild']].rem_channels.includes(results[i]['removal_req_chan']))
					{
						temp_contests_struct[results[i]['guild']].rem_channels.push(results[i]['removal_req_chan'])
					}

					if     (results[i]['state'] == 0){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['start_tsp']}
					else if(results[i]['state'] == 1){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['vote_tsp']}
					else if(results[i]['state'] == 2){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['end_tsp']}
					else                             { temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = 0}

					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].announcement = results[i]['announcement_chan']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].management   = results[i]['management_chan']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].emoji        = results[i]['contest_emoji']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].system       = results[i]['contest_sys']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].id           = results[i]['contest']
					temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].state        = results[i]['state']
				}
			}
			else
			{
				temp_contests_struct[results[i]['guild']] = {}
				temp_contests_struct[results[i]['guild']].channels     = [results[i][ 'submission_chan']]
				temp_contests_struct[results[i]['guild']].rem_channels = [results[i]['removal_req_chan']]
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']] = {}

				if     (results[i]['state'] == 0){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['start_tsp']}
				else if(results[i]['state'] == 1){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['vote_tsp']}
				else if(results[i]['state'] == 2){ temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = results[i]['end_tsp']}
				else                             { temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].tsp = 0}

				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].announcement = results[i]['announcement_chan']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].management   = results[i]['management_chan']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].emoji        = results[i]['contest_emoji']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].system       = results[i]['contest_sys']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].id           = results[i]['contest']
				temp_contests_struct[results[i]['guild']][results[i]['submission_chan']].state        = results[i]['state']
			}

		}

		contests_struct = temp_contests_struct

		// Load Contests
		try {
			Object.keys(temp_contests_struct).forEach( guild_id=> {
				temp_contests_struct[guild_id].channels.forEach(channel_id => { 

					current_tsp = Date.now()
					target_tsp	= new Date(temp_contests_struct[guild_id][channel_id].tsp)

					if (current_tsp > target_tsp){
						if      (temp_contests_struct[guild_id][channel_id].state == 0){contest_start     (guild_id, channel_id, temp_contests_struct[guild_id][channel_id].announcement, temp_contests_struct[guild_id][channel_id].system, temp_contests_struct[guild_id][channel_id].id, temp_contests_struct[guild_id][channel_id].state, temp_contests_struct[guild_id][channel_id].emoji)}
						else if (temp_contests_struct[guild_id][channel_id].state == 1){voting_start      (guild_id, channel_id, temp_contests_struct[guild_id][channel_id].announcement, temp_contests_struct[guild_id][channel_id].system, temp_contests_struct[guild_id][channel_id].id, temp_contests_struct[guild_id][channel_id].state, temp_contests_struct[guild_id][channel_id].emoji)}
						else if (temp_contests_struct[guild_id][channel_id].state == 2){contest_end       (guild_id, channel_id, temp_contests_struct[guild_id][channel_id].announcement, temp_contests_struct[guild_id][channel_id].system, temp_contests_struct[guild_id][channel_id].id, temp_contests_struct[guild_id][channel_id].state, temp_contests_struct[guild_id][channel_id].emoji)}
						else                                                           {contest_queue_next(guild_id, channel_id, temp_contests_struct[guild_id][channel_id].announcement, temp_contests_struct[guild_id][channel_id].system, temp_contests_struct[guild_id][channel_id].id, temp_contests_struct[guild_id][channel_id].state, temp_contests_struct[guild_id][channel_id].emoji)}
					}
				})
			});
		} catch (error){
			log_error('PL-Contest', 'constest loop', 'constest loop', error.stack)
		}
	
		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'constest loop', 'constest loop', error.stack)
	}
}

async function contest_start(guild_id, sub_channel_id, ann_channel_id, system, id, state, emoji){
	try {

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_con + ' WHERE id = ?', [id])

		mysql_conn.end()

		if(results.length > 0)
		{

			const guild       = client.guilds .cache.get(guild_id);
			const sub_channel = guild.channels.cache.get(sub_channel_id);
			const ann_channel = guild.channels.cache.get(ann_channel_id);

			//Get message
			const ids = results[0]['link'].slice("https://discord.com/channels/".length).split("/")
			const msg_channel 	= guild.channels.cache.get(ids[1])
			
			// You don't wanna know how this works trust me
			msg_channel.messages.fetch(ids[2]).then(con_message => {
				ann_channel.send(`<@&${config[guild_id].common.roles.notification}>\n` + con_message.content.replace(/<t:.*>/, `<t:${new Date(results[0]['vote_tsp']) / 1000}:F>`)).then(msg => {
					sub_channel.permissionOverwrites.edit(config[guild_id].common.roles.everybody, { SendMessages: true  }).then(msg => {
						sub_channel.send("----- SUBMISSIONS BELOW ------").then(async msg => {
							
							// Update end message column to contests table
							var mysql_conn_2 = await  mysql.createConnection(mysq_data);
							const [results_upd, fields_upd] = await mysql_conn_2.execute('UPDATE ' + db_table_con + ' SET start_msg = ? WHERE id = ?', [msg.id, id])
							mysql_conn_2.end()

						}).catch(error => {log_error('PL-Contest', 'contest start', 'sub below msg'              , error.stack)})
					}).catch(    error => {log_error('PL-Contest', 'contest start', 'perm override'              , error.stack)})
				}).catch(        error => {log_error('PL-Contest', 'contest start', 'post announcement'          , error.stack)})
			}).catch(            error => {log_error('PL-Contest', 'contest start', 'fetch announcement channel' , error.stack)})
		}
		else
		{
			log_error('PL-Contest', 'contest start', 'contest database entry missing', error.stack)
			return
		}

		//Update Contest Sys State
		log_info('PL-Contest', '', 'start contest', id)
		update_con_state(system, state +  1)

	} catch (error){
		log_error('PL-Contest', 'contest start', 'contest start', error.stack)
	}
}

async function voting_start(guild_id, sub_channel_id, ann_channel_id, system, id, state, emoji){
	try {

		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_con + ' WHERE id = ?', [id])

		mysql_conn.end()

		const guild       = client.guilds .cache.get(guild_id);
		const sub_channel = guild.channels.cache.get(sub_channel_id);
		const ann_channel = guild.channels.cache.get(ann_channel_id);

		if(results.length > 0)
		{
			sub_channel.messages.fetch(results[0]['start_msg']).then(start_msg => {
				
				ann_channel.send({ content: `<@&${config[guild_id].common.roles.notification}>\n Hey Photographers! 📸 \n Voting has now begun… please take a look starting here:${start_msg.url}\n\n You can vote for as many submissions as you like, but you cannot vote for your own \n\n Voting will end on <t:${new Date(results[0]['end_tsp']) / 1000}:F>`})
				.catch(error => {log_error('PL-Contest', 'voting start', 'voting start message not sent' , error.stack)})

				sub_channel.send(`----- VOTE ABOVE -----\n You can start scrolling down from:\n${start_msg.url}`).then(async msg => {

					sub_channel.permissionOverwrites.edit(config[guild_id].common.roles.everybody, { SendMessages: false  })
					.catch(error => {log_error('PL-Contest', 'voting start', 'voting start message not sent' , error.stack)})

					var mysql_conn_2 = await  mysql.createConnection(mysq_data);

					const [results_upd, fields_upd] = await mysql_conn_2.execute('UPDATE ' + db_table_con + ' SET end_msg = ? WHERE id = ?', [msg.id, id])
					
					// React to all messages
					const [results_upv, fields_upv] = await mysql_conn_2.execute('SELECT * From ' + db_table_sub + ' WHERE contest = ?', [id])

					results_upv.forEach(result => { 
						sub_channel.messages.fetch(result.message).then( msg => {
							msg.react(emoji).catch(error => {log_error('PL-Contest', 'voting start', 'react to message' , error.stack)})
						}).catch(                  error => {log_error('PL-Contest', 'voting start', 'fetch message'    , error.stack)})
					})
					mysql_conn_2.end()

				}).catch(error => {log_error('PL-Contest', 'voting start', 'send voting start message'   , error.stack)})
			}).catch(    error => {log_error('PL-Contest', 'voting start', 'fetch contest start message' , error.stack)})
		}
		else
		{
			log_error('PL-Contest', 'voting start', 'contest database entry missing', error.stack)
			return
		}

		//Update Contest Sys State
		log_info('PL-Contest', '', 'start contest voting', id)
		update_con_state(system, state +  1)
	} catch (error){
		log_error('PL-Contest', 'voting start', 'voting start', error.stack)
	}
}

async function contest_end(guild_id, sub_channel_id, ann_channel_id, system, id, state, emoji){
	try {

		const guild       = client.guilds .cache.get(guild_id);
		const sub_channel = guild.channels.cache.get(sub_channel_id);
		const ann_channel = guild.channels.cache.get(ann_channel_id);

		ann_channel.send(`\n Hey Photographers! 📸 \n Voting is completed.\n\n The results will be announced shorty.`)
		.catch(error => {log_error('PL-Contest', 'contest end', 'react to message' , error.stack)})


		var mysql_conn = await  mysql.createConnection(mysq_data);

		// Count Votes
		const [results_con, fields_con] = await mysql_conn.execute('SELECT * From ' + db_table_sub + ' WHERE contest = ?', [id])
		// Set contest as completed
		const [results_upd, fields_upd] = await mysql_conn.execute('UPDATE ' + db_table_con + ' SET completed = 1 WHERE id = ?', [id])
		mysql_conn.end()

		for (let i = 0; i < results_con.length; i++) {
			try {
				msg = await sub_channel.messages.fetch(results_con[i]["message"])
				setTimeout(set_votes, i * 1000, msg.reactions.cache.get(emoji.split(":")[2].replace(">","")).count, msg.id);
			} catch (error){
				ann_channel.send(`<@345276559038611466>\n Failed to fetch message ${results_con[i]["message"]}.`)
				.catch(error => {log_error('PL-Contest', 'contest end', 'react to message' , error.stack)})
				
				log_error('PL-Contest', 'contest end', `Failed to fetch message ${results_con[i]["message"]}`, error.stack)
			}
		}

		var mysql_conn = await  mysql.createConnection(mysq_data);
		
		mysql_conn.end()

		//Update Contest Sys State
		log_info('PL-Contest', '', 'end contest', id)
		update_con_state(system, state +  1)

		setTimeout(contest_result_embed, results_con.length * 1000, ann_channel, {"id":id,"page":null,"pages":null}, true);

	} catch (error){
		log_error('PL-Contest', 'contest end', 'contest end', error.stack)
	}
}

async function set_votes(votes, msgid){
	try {
		var mysql_conn = await  mysql.createConnection(mysq_data);
		const [results, fields] = await mysql_conn.execute('UPDATE ' + db_table_sub + ' SET votes = ? WHERE message = ?', [votes, msgid])

        var success = false
        if (results["affectedRows"] == 1 && results["changedRows"] == 1){ success = true }

		log_data = `msg ${msgid} votes ${votes}`
		log_info('PL-Contest', 'contest counting votes', 'set_votes', log_data)
		mysql_conn.end()
	} catch (error){
		log_error('PL-Contest', 'contest counting votes', 'set_votes', error.stack)
	}
}

async function contest_queue_next(guild_id, sub_channel_id, ann_channel_id, system, id, state, emoji){
	try {
		// Get Next Conrest
		
		var mysql_conn = await  mysql.createConnection(mysq_data);

		const [results_con, fields_con] = await mysql_conn.execute('SELECT id FROM ' + db_table_con + ' WHERE contest_sys = ? AND completed = 0 AND start_tsp > CURRENT_TIMESTAMP ORDER BY start_tsp ASC', [system])

		if(results_con.length > 0)
		{
			const [results, fields] = await mysql_conn.execute('UPDATE ' + db_table_sta + ' SET contest = ? WHERE contest_sys = ?', [results_con[0]["id"], system])

			log_info('PL-Contest', '', 'queue contest', results_con[0]["id"])
			update_con_state(system, state +  1)			
		}
		else
		{
			log_warn('PL-Contest', 'contest queue next', '', `Contest Queue for system ${system} is empty`)
		}

		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest queue next', 'contest queue next', error.stack)
	}
}


async function update_con_state(system, state){
	try {
		if(state > 3){state = 0} //Reset

		var mysql_conn = await  mysql.createConnection(mysq_data);

		// Update status column to statis table
		const [results_sta, fields_sta] = await mysql_conn.execute('UPDATE ' + db_table_sta + ' SET state = ? WHERE contest_sys = ?', [state, system])
		mysql_conn.end()
		
	} catch (error){
		log_error('PL-Contest', 'update state', 'database error', error.stack)
	}
}

async function contest_result_embed(interaction, args, send){
	/*
	┌──────────────────────┐
	│ Contest Leaderboard  │
	│ Page: 1/3            │
	│ Votes   User    Link │
	│ 100     User1   Link │
	│  99     User2   Link │
	│  98     User3   Link │
	│  97     User4   Link │
	│  96     User5   Link │
	│  95     User6   Link │
	│  94     User7   Link │
	│  93     User8   Link │
	│  92     User9   Link │
	│  91     UserA   Link │
	│[<con_id>|1|3]        │
	└──────────────────────┘
	┌───┐┌───┐┌───┐┌───┐┌───┐
	│ « ││ < ││ _ ││ » ││ > │
	└───┘└───┘└───┘└───┘└───┘
	└ Page Change  
	*/

	try {
		if(args.page	== null){args.page	= 1}
		if(args.pages	== null){args.pages	= 1}
		let col1 	= ""
		let col2 	= ""
		let col3 	= ""

		var mysql_conn = await  mysql.createConnection(mysq_data)

		const [results_sta, fields_sta] = await mysql_conn.execute('SELECT COUNT(*) FROM ' + db_table_sub + ' WHERE contest = ?', [args.id])
		
		if(results_sta[0]['COUNT(*)'] != 0)
		{		
			args.pages = Math.ceil(results_sta[0]['COUNT(*)'] / 10).toFixed();

			const [results, fields] = await mysql_conn.execute('SELECT * FROM ' + db_table_sub + ' WHERE contest = ? ORDER BY votes DESC, id ASC LIMIT 10 OFFSET ' + ((args.page - 1) * 10), [args.id])
			
			if(results.length < 1){
				log_warn('PL-Contest', 'contest results show', '', 'contest leaderboard  empty')
				return null
			}

			for (let i = 0; i < results.length && i < 10; i++) {	
				col1 = col1 + `${results[i]["votes"]}\n`
				col2 = col2 + `<@${results[i]["user"]}>\n`
				col3 = col3 + `https://discord.com/channels/${interaction.guildId}/${results[i]['channel']}/${results[i]["message"]}\n`
				
			}

			contest_sys_first	= new Discord.ButtonBuilder().setCustomId('contest_res_first').setLabel('<<').setStyle(Discord.ButtonStyle.Primary)
			contest_sys_prev	= new Discord.ButtonBuilder().setCustomId('contest_res_prev' ).setLabel('<') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_mid		= new Discord.ButtonBuilder().setCustomId('contest_res_mid'  ).setLabel('─') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_next	= new Discord.ButtonBuilder().setCustomId('contest_res_next' ).setLabel('>') .setStyle(Discord.ButtonStyle.Primary)
			contest_sys_last	= new Discord.ButtonBuilder().setCustomId('contest_res_last' ).setLabel('>>').setStyle(Discord.ButtonStyle.Primary)

			let contest_sys_embed = new  Discord.EmbedBuilder()
			.setTitle("Contest Leaderboard")
			.setColor(config[interaction.guildId].common.embed)
			.setDescription(`Page: ${args.page}/${args.pages}`)
			.setFooter({ text: `[${args.id}|${args.page}|${args.pages}]`})
			.addFields(
				{name:'Votes',       value: col1, inline: true},
				{name:'User',        value: col2, inline: true},
				{name:'Submission',  value: col3, inline: true},
			)

			if(send) // Reply with embed 
			{
				interaction.send({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Contest', 'contest results show', 'send', error.stack)
				})
				
			}
			else // Update embed 
			{
				interaction.update({embeds: [contest_sys_embed], components: [{type: 1,  components:[contest_sys_first,contest_sys_prev,contest_sys_mid,contest_sys_next,contest_sys_last]}]}).catch(error => {
					log_error('PL-Contest', 'contest results show', 'interaction update', error.stack)			
				})
			}
		}
		else
		{
			log_warn('PL-Contest', 'contest results show', '', 'contest leaderboard  empty')
		}
		mysql_conn.end()

	} catch (error){
		log_error('PL-Contest', 'contest results show', 'contest embed main', error.stack)
		return null
	}
}

function btn_contest_results (interaction){
	try {
		let button	= interaction.customId.split("_")
		let footer	= interaction.message.embeds[0].footer.text.replace("[","").replace("]","").split("|")

		if(button.length != 3 && footer.length != 3){
			interaction.reply({ content:"Unsupported Command", ephemeral: true }).catch(error => {
				log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)
			})  
			return
		}

		args = {
			"id"	:	parseInt(footer[0]),
			"page"	: 	parseInt(footer[1]),
			"pages"	:	parseInt(footer[2])
		}

		if(button[2] == "first"){
			if(args.page > 1){args.page = 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "next")
		{
			if(args.page < args.pages){args.page = args.page + 1}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "mid")
		{
			interaction.reply({ content:"This button is just for alignment.", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)})
			return
		}
		else if(button[2] == "prev")
		{
			if(args.page > 1){args.page = args.page - 1}
			else
			{
				interaction.reply({content:"Already on first page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)})
				return
			}	
		}
		else if(button[2] == "last")
		{
			if(args.page != args.pages){args.page = args.pages}
			else
			{
				interaction.reply({content:"Already on last page", ephemeral: true }).catch(error => {log_error('PL-Contest', 'contest res button', 'interaction reply', error.stack)})
				return
			}	
		}

		if(button[2] == "first" || button[2] == "next" || button[2] == "prev" || button[2] == "last")
		{
			embed = contest_result_embed(interaction, args, false)
		}

	} catch (error){
		log_error('PL-Contest', 'contest res button', 'contest res button', error.stack)
	}
}

async function issue_req_rem (interaction){
	try {
		let description	= interaction.options.getString('description' )
		let link	    = interaction.options.getString('link')	

		let rem_sub_guild	= link.split('/')[4];
		let rem_sub_chan	= link.split('/')[5];
		let rem_sub_msg		= link.split('/')[6];

		var mysql_conn = await  mysql.createConnection(mysq_data);
		const [results, fields] = await mysql_conn.execute('SELECT * From ' + db_table_sys + ' WHERE guild = ? and submission_chan = ?', [rem_sub_guild, rem_sub_chan])
		mysql_conn.end()

		if(results.length > 0)
		{
			// Todo Check for Legit Sub Channel
			let removal_sub_channel = client.channels.cache.get(rem_sub_chan);
			let removal_req_channel = client.channels.cache.get(results[0]['removal_req_chan']);
			let removal_message     = await removal_sub_channel.messages.fetch(rem_sub_msg)

			//Image Embed
			let authordata = {
				name: `Removal Request [Pending]`, 
				iconURL: removal_message.author.displayAvatarURL()
			}

			let removal_req_embed = new Discord.EmbedBuilder()
				.setColor("#FF0000")
				.setImage(removal_message.attachments.first().url)
				.setAuthor(authordata)
				.addFields(
					{name:'User:',                value: `<@${removal_message.author.id}>`, inline: true },
					{name:'Cahnnel:',             value: `<#${removal_message.channelId}>`, inline: true },
					{name:'Link:',                value: `${link}`,                         inline: true },
					{name:'Removal Description:', value: `${description}`,                  inline: false}
				)

			removal_req_channel.send({ embeds: [removal_req_embed] }).then( msg => {msg.react(results[0]['contest_emoji'])})
			interaction.reply({ content:`Message Removal Issused See: <#${rem_sub_chan}>`, ephemeral: true })
		}
		else
		{
			interaction.reply({ content:`You tried to remove a message in a non contest submission channel`})
		}


	} catch (error){
		log_error('PL-Contest', 'Issue Removal Request', 'issue_req_rem', error.stack)
	}
}

async function rem_req_rem (reaction){
	try {

		// sql request to get emoji and removal count by submission channel id

		old_embed = reaction.message.embeds[0]

		rem_que_usr  = old_embed.data.fields[0]["value"]
		link         = old_embed.data.fields[2]["value"]
		rem_que_desc = old_embed.data.fields[3]["value"]

		let rem_sub_guild	= link.split('/')[4];
		let rem_sub_chan 	= link.split('/')[5];
		let rem_sub_msg  	= link.split('/')[6];

		var mysql_conn = await  mysql.createConnection(mysq_data);
		const [results, fields] = await mysql_conn.execute('SELECT * From ' + db_table_sys + ' WHERE guild = ? and submission_chan = ?', [rem_sub_guild, rem_sub_chan])
		mysql_conn.end()

		if(results.length > 0)
		{
			if (reaction.count >= results[0]["removal_cnt"] &&  results[0]["contest_emoji"].includes(reaction.emoji.id))
			{
				// Todo Check for Legit Sub Channel
				let removal_sub_channel = client.channels.cache.get(rem_sub_chan);
				let removal_que_channel = client.channels.cache.get(results[0]["removal_req_chan"]);
				let removal_res_channel = client.channels.cache.get(results[0]["removal_res_chan"]);
				let removal_message     = await removal_sub_channel.messages.fetch(rem_sub_msg)

				//Image Embed
				let authordata = {
					name: `Removal Request [Approved]`, 
					iconURL: old_embed.data.author.icon_url
				}

				let removal_req_embed = new Discord.EmbedBuilder()
					.setColor("#00FF00")
					.setAuthor(authordata)
					.setImage( old_embed.data.image.url)
					.addFields(old_embed.data.fields)


				reaction.message.edit({ embeds: [removal_req_embed] })
				reaction.message.reactions.removeAll()

				let removal_res_embed = new Discord.EmbedBuilder()
					.setColor(config[reaction.message.guildId].common.embed)
					.setAuthor({name: `Removal of contest submission`})
					.addFields({name:'Reason:', value: `${rem_que_desc}`})
					.setImage( old_embed.data.image.url)

				removal_res_channel.send({content: rem_que_usr, embeds: [removal_res_embed] })

				removal_message.delete()
			}
		}

	//Remove Message
	} catch (error){
		log_error('PL-Contest', 'Approve Removal Request', 'rem_req_rem', error.stack)
	}
}